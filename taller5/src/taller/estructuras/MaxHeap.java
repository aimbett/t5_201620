package taller.estructuras;

import java.util.Comparator;

public class MaxHeap<T> implements IHeap<T> {

	
	private Comparator comparable;
	private int posicion;
	private T[] maHeap;
	
	
	public MaxHeap(Comparator<T> pComparable, int tamMax) {
		// TODO Auto-generated constructor stub
		comparable = pComparable;
		maHeap = (T[]) new Object[tamMax+1];
		posicion = 0;
	}
	@Override
	public void add(T elemento) {
		// TODO Auto-generated method stub
		boolean agrego = false;
		if(posicion== 0)
		{
			maHeap[posicion+1] = elemento;
			posicion=2;
			
		}
		else{
			maHeap[posicion++]= elemento;
			siftUp();
		}
		
		
		
	}

	@Override
	public T peek() {
		// TODO Auto-generated method stub
		return maHeap[1];
	}

	@Override
	public T poll() {
		// TODO Auto-generated method stub
		T raiz = maHeap[1];
		maHeap[1] = maHeap[posicion];
		posicion--;
		siftDown();
		return raiz;
	}

	@Override
	public int size() {
		// TODO Auto-generated method stub
		return posicion;
	}

	@Override
	public boolean isEmpty() {
		// TODO Auto-generated method stub
		return posicion==0;
	}

	@Override
	public void siftUp() {
		// TODO Auto-generated method stub
		int pos = posicion-1;
		while(pos>0 && comparable.compare(maHeap[pos/2],maHeap[pos])<0){
			T y = maHeap[pos];
			maHeap[pos]=maHeap[pos/2];
			maHeap[pos/2] = y;
			pos = pos/2;
		}
		
	}

	@Override
	public void siftDown() {
		// TODO Auto-generated method stub
		int maximo = 1;
	
		
		
	}
	
	public void swap(int a, int b){
	
		T temp = maHeap[a];
		maHeap[a] = maHeap[b];
		maHeap[b] = temp;
	}

}
