package taller.estructuras;

public class Nodo<T> {

	private T elemento;

	private Nodo<T> izquierda;

	private Nodo<T> derecha;

	private Nodo<T> padre;

	public Nodo(T elem, Nodo<T> pPadre)
	{
		elemento = elem;
		padre = pPadre;
	}
	
	public boolean esHoja()
	{
		return izquierda == null && derecha == null;
	}
	public Nodo<T> darIzquierdo()
	{
		return izquierda;
	}
	public Nodo<T> darDerecho()
	{
		return derecha;
	}
}
