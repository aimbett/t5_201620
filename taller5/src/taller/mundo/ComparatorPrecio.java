package taller.mundo;

import java.util.Comparator;

public class ComparatorPrecio implements Comparator<Pedido> {

	@Override
	public int compare(Pedido o1, Pedido o2) {
		// TODO Auto-generated method stub
		return Double.compare(o1.getPrecio(), o2.getPrecio());
	}

}
