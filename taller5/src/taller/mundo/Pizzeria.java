package taller.mundo;

import taller.estructuras.MaxHeap;
import taller.estructuras.MinHeap;

public class Pizzeria 
{	
	// ----------------------------------
    // Constantes
    // ----------------------------------
	
	/**
	 * Constante que define la accion de recibir un pedido
	 */
	public final static String RECIBIR_PEDIDO = "RECIBIR";
	/**
	 * Constante que define la accion de atender un pedido
	 */
	public final static String ATENDER_PEDIDO = "ATENDER";
	/**
	 * Constante que define la accion de despachar un pedido
	 */
	public final static String DESPACHAR_PEDIDO = "DESPACHAR";
	/**
	 * Constante que define la accion de finalizar la secuencia de acciones
	 */
	public final static String FIN = "FIN";
	
	// ----------------------------------
    // Atributos
    // ----------------------------------
	
	/**
	 * Heap que almacena los pedidos recibidos
	 */
	// TODO 
	private MaxHeap<Pedido> heapPedidos;
	/**
	 * Getter de pedidos recibidos
	 */
	// TODO 
	public MaxHeap<Pedido> darHeapPedidos()
	{
		return heapPedidos;
	}
 	/** 
	 * Heap de elementos por despachar
	 */
	// TODO 
	private MinHeap<Pedido> heapDespachos;
	/**
	 * Getter de elementos por despachar
	 */
	// TODO 
	public MinHeap<Pedido> darHeapDespachos()
	{
		return heapDespachos;
	}
	
	// ----------------------------------
    // Constructor
    // ----------------------------------

	/**
	 * Constructor de la case Pizzeria
	 */
	public Pizzeria()
	{
		// TODO 
		heapPedidos = new MaxHeap<Pedido>(new ComparatorPrecio(), 100);
		heapDespachos = new MinHeap<Pedido>(new ComparatorCercania(),100);
	}
	
	// ----------------------------------
    // Métodos
    // ----------------------------------
	
	/**
	 * Agrega un pedido a la cola de prioridad de pedidos recibidos
	 * @param nombreAutor nombre del autor del pedido
	 * @param precio precio del pedido 
	 * @param cercania cercania del autor del pedido 
	 */
	public void agregarPedido(String nombreAutor, double precio, int cercania)
	{
		// TODO 
		heapPedidos.add(new Pedido(precio, nombreAutor, cercania));
	}
	
	// Atender al pedido más importante de la cola
	
	/**
	 * Retorna el proximo pedido en la cola de prioridad o null si no existe.
	 * @return p El pedido proximo en la cola de prioridad
	 */
	public Pedido atenderPedido()
	{
		// TODO 
		Pedido pedido = heapPedidos.poll();
		heapDespachos.add(pedido);
		return  pedido;
	}

	/**
	 * Despacha al pedido proximo a ser despachado. 
	 * @return Pedido proximo pedido a despachar
	 */
	public Pedido despacharPedido()
	{
		// TODO 
	
	    return  heapDespachos.poll();
	}
	
	 /**
     * Retorna la cola de prioridad de pedidos recibidos como un arreglo. 
     * @return arreglo de pedidos recibidos manteniendo el orden de la cola de prioridad.
     */
     public Pedido [] pedidosRecibidosList()
     {
        // TODO 
    	 Pedido [] listaPedidos = new Pedido[heapPedidos.size()];
    	 for(int i=0;i<heapPedidos.size();i++)
    	 {
    		 listaPedidos[i] = heapPedidos.poll();
    	 }
        return  listaPedidos;
     }
     
      /**
       * Retorna la cola de prioridad de despachos como un arreglo. 
       * @return arreglo de despachos manteniendo el orden de la cola de prioridad.
       */
     public Pedido [] colaDespachosList()
     {
         // TODO 
    	 Pedido[] listaDespacho = new Pedido[heapDespachos.size()];
    	 for (int i = 0; i < heapDespachos.size(); i++)
    	 {
			listaDespacho[i] =heapDespachos.poll();
			
		}
         return  listaDespacho;
     }
}
