package taller.mundo;

import java.util.Comparator;

public class Pedido implements Comparator<Pedido>
{

	// ----------------------------------
	// Atributos
	// ----------------------------------

	/**
	 * Precio del pedido
	 */
	private double precio;
	
	/**
	 * Autor del pedido
	 */
	private String autorPedido;
	
	/**
	 * Cercania del pedido
	 */
	private int cercania;
	
	
	
	// ----------------------------------
	// Constructor
	// ----------------------------------
	
	/**
	 * Constructor del pedido
	 * TODO Defina el constructor de la clase
	 */
	public Pedido(double pPrecio, String pAutorPedido, int pCercania)
	{
		precio = pPrecio;
		autorPedido = pAutorPedido;
		cercania = pCercania;
	}
	
	// ----------------------------------
	// Métodos
	// ----------------------------------
	
	/**
	 * Getter del precio del pedido
	 */
	public double getPrecio()
	{
		return precio;
	}
	
	/**
	 * Getter del autor del pedido
	 */
	public String getAutorPedido()
	{
		return autorPedido;
	}
	
	/**
	 * Getter de la cercania del pedido
	 */
	public int getCercania() {
		return cercania;
	}

	
	
	public int compararPrecio(Object o)
	{
		Pedido pedido = (Pedido) o;
		if(precio<pedido.getPrecio())
		{
			return -1;
		}
		else if(precio == pedido.getPrecio())
		{
			return 0;
		}
		else
		{
			return 1;
		}
	}
	
	public int compararCercania(Object o)
	{
		Pedido pedido = (Pedido) o;
		if(cercania<pedido.getCercania())
		{
			return -1;
		}
		else if(cercania == pedido.getCercania())
		{
			return 0;
		}
		else
		{
			return 1;
		}
	}

	@Override
	public int compare(Pedido arg0, Pedido arg1) {
		// TODO Auto-generated method stub
		return 0;
	}
	
	// TODO 
}
