package taller.estructuras;

import java.util.Comparator;

public class MinHeap<T> implements IHeap<T> {

	private Comparator comparable;
	private int posicion;
	private T[] miHeap;
	
	
	public MinHeap(Comparator<T> pComparable, int tamMax) {
		// TODO Auto-generated constructor stub
		comparable = pComparable;
		miHeap = (T[]) new Object[tamMax+1];
		posicion = 0;
	}
	@Override
	public void add(T elemento) {
		// TODO Auto-generated method stub
		boolean agrego = false;
		if(posicion== 0)
		{
			miHeap[posicion+1] = elemento;
			posicion=2;
			
		}
		else{
			miHeap[posicion++]= elemento;
			siftUp();
		}
		
		
		
	}

	@Override
	public T peek() {
		// TODO Auto-generated method stub
		return miHeap[1];
	}

	@Override
	public T poll() {
		// TODO Auto-generated method stub
		T raiz = miHeap[1];
		miHeap[1] = miHeap[posicion];
		posicion--;
		siftDown();
		return raiz;
	}

	@Override
	public int size() {
		// TODO Auto-generated method stub
		return posicion;
	}

	@Override
	public boolean isEmpty() {
		// TODO Auto-generated method stub
		return posicion==0;
	}

	@Override
	public void siftUp() {
		// TODO Auto-generated method stub
		int pos = posicion-1;
		while(pos>0 && comparable.compare(miHeap[pos/2],miHeap[pos])>0){
			T y = miHeap[pos];
			miHeap[pos]=miHeap[pos/2];
			miHeap[pos/2] = y;
			pos = pos/2;
		}
		
	}

	@Override
	public void siftDown() {
		// TODO Auto-generated method stub
		int maximo = 1;
	
		
		
	}
	
	public void swap(int a, int b){
	
		T temp = miHeap[a];
		miHeap[a] = miHeap[b];
		miHeap[b] = temp;
	}


}
